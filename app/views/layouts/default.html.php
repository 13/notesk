<!DOCTYPE html>
<html lang="en">
<head>
	<?php echo $this->html->charset();?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title>Notesk<?php //echo $this->title(); ?></title>

	<!-- Custom styles for this template -->
	<?php echo $this->html->style(array('bootstrap.min', 'app')); ?>
	<?php echo $this->html->script(array(
		'//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js',
		'//ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js',
		'bootstrap.min',
		'app',
	)); ?>
	<?php echo $this->scripts(); ?>
	<?php echo $this->styles(); ?>
	
	<!-- Just for debugging purposes. Don't actually copy this line! -->
	<!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="container fill">
		<?php if ($user) { ?>
			<a id="logout-link" href="<?php echo lithium\net\http\Router::match('Users::logout');?>"><span style="font-size: 13px; vertical-align: 3px;">logout</span> <span class="glyphicon glyphicon-log-out"></span></a>
		<?php } ?>
		<?php /*echo $this->html->link('Notesk', 'Users::login', array('class' => 'logo'));*/ ?>
		<div class="content fill">
			<?php echo $this->content(); ?>
		</div>
	</div>
</body>
</html>
