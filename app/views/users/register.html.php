<h1>Register</h1>

<?php if ($error) { ?>
	<h4>Sorry, user with email <b><?= $data['email'] ?></b> already exists.</h4>
<?php } ?>

<div class="well well-large">
	<?= $this->form->create() ?>
		<?= $this->form->field('email') ?>
		<?= $this->form->field('password',array('type'=>'password')) ?>
		<?= $this->form->submit('Register', array('class' => 'btn btn-default')) ?>
	<?= $this->form->end() ?>
</div>
