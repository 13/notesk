<form id="notepad-form" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
	<div class="notepad-notices">
		<div id="notepad-notice-edited" class="notepad-notice"><span class="glyphicon glyphicon-pencil"></span></div>
		<div id="notepad-notice-saved" class="notepad-notice"><span class="glyphicon glyphicon-ok"></span> saved</div>
	</div>
	<div class="notes-wrapper">
		<textarea id="notes" name="notes" class="form-control"><?= "\n" ?><?= $user->notes ?></textarea>
	</div>
	<div id="under-textarea">
		<input class="btn btn-special" type="submit" name="save_notepad" value="Save" />
		<div id="edited-date-wrapper">
			edited <span id="edited-date"><?= $user->notes_updated_formatted ?></span>
		</div>
	</div>
</form>
