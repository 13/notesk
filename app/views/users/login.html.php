<?php if ($noauth) { ?>
	<h4>Invalid email or password.</h4>
<?php } ?>

<div class="col-sm-3 login-form-wrapper">
	<?= $this->form->create() ?>
		<?= $this->form->field('email') ?>
		<?= $this->form->field('password', array('type'=>'password')) ?>
		<?= $this->form->submit('Login', array('class' => 'btn btn-special')) /*?> or <?php echo $this->html->link('Register', 'Users::register');*/ ?>
	<?= $this->form->end() ?>
</div>
<div class="col-sm-9">
	<div class="welcome-box panel panel-default">
		<div class="panel-body">
			<h2>Welcome to notesk!</h2>
			<div>
				<p>Simple online notepad.</p>
				<p>Service is currently being developed.</p>
				<p>For any questions please write here <img src="/img/mail.jpg" alt="I try to protect my email against spam" style="vertical-align:top;" /></p>
			</div>

			<hr>

			<div class="register-form-wrapper">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Demo</h3>
					</div>
					<div class="panel-body">
						<div style="position:relative; height:200px">
							<form id="notepad-form" action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
								<textarea id="notes" name="notes" class="form-control" style="top: 10px;"><?= "\n" ?><?= $demo['notes'] ?></textarea>
								<div id="under-textarea">
									<input class="btn btn-special" type="submit" name="save_notepad" value="Save" />
									<!-- (cmd or ctrl) + s -->
									<div id="edited-date-wrapper"<?php if (!$demo['date']) { ?> class="hide"<?php } ?>>
										edited <span id="edited-date"><?= $demo['date'] ?></span>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>

			<hr>

			<div class="register-form-wrapper col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">New to Notesk? Sign up<!--Very simple registration--></h3>
					</div>
					<div class="panel-body">
						<?= $this->form->create(null, array('action' => 'register')) ?>
							<?= $this->form->field('email') ?>
							<?= $this->form->field('password',array('type'=>'password')) ?>
							<?= $this->form->submit('Create notepad', array('class' => 'btn btn-special')) ?>
						<?= $this->form->end() ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
