<?php

namespace app\controllers;

use app\models\Users;
use lithium\action\DispatchException;
use lithium\security\Auth;

class UsersController extends \lithium\action\Controller {

	public function login() {

		if ($user = Auth::check('default')) {
			//User is authenticated, redirect to notepad
			return $this->redirect(array('Users::notepad'));
		}

		$user = Auth::check('default');
		$noauth = false;

		if ($this->request->data) {
			if (isset($this->request->data['notes'])) {
				$now = new \DateTime();

				setcookie('demo_notes', $this->request->data['notes'], time() + 8640000);
				setcookie('demo_date', $now->format('Y-m-d H:i:s'), time() + 8640000);

				if ($this->request->is('ajax')) {
					return $now->format('j F Y \a\t H:i');
				}
				return $this->redirect(array('Users::login'));
			}

			if (Auth::check('default', $this->request)) {
				return $this->redirect('Users::notepad');
			}
			$noauth = true;
		}

		if (isset($_COOKIE['demo_date'])) {
			$notes_updated = new \DateTime($_COOKIE['demo_date']);
			$demo['date'] = $notes_updated->format('j F Y \a\t H:i');
		} else {
			$demo['date'] = false;
		}

		if (isset($_COOKIE['demo_notes'])) {
			$demo['notes'] = $_COOKIE['demo_notes'];
		} else {
			$demo['notes'] = '';
		}

		return compact('noauth', 'user', 'demo');
	}

	public function logout() {
		
		Auth::clear('default');
		return $this->redirect('Users::login');
	}

	public function register() {
		
		$user = Auth::check('default');
		$register = NULL;
		$error = false;

		if ($this->request->data) {
			if (Users::find('first', array('conditions' => array('email' => $this->request->data['email'])))) {
				$error = true;
			} else {
				$register = Users::create();
				$register->email = $this->request->data['email'];
				$register->token = '';
				$register->notes = '';
				$register->password = \lithium\security\Password::hash($this->request->data['password']);
				if ($register->save()) {
					Auth::check('default', $this->request);
					$this->redirect(array('Users::notepad'));
				} else {
					die('registration error');
				}
			}
		}
		$data = $this->request->data;

		return compact('register', 'data', 'user', 'error');
	}

	// public function restore_password() {

	// }

	public function notepad() {

		if (!($user = Auth::check('default'))) {
			//User is not authenticated, redirect to login
			return $this->redirect(array('Users::login'));
		}

		$user = Users::find($user['id']);

		if ($this->request->data) {
			$now = new \DateTime();
			$user->notes_updated = $now->format('Y-m-d H:i:s');
			$user->save($this->request->data);
			if ($this->request->is('ajax')) {
				return $now->format('j F Y \a\t H:i');
			}
			return $this->redirect(array('Users::notepad'));
		}

		$notes_updated = new \DateTime($user->notes_updated);
		$user->notes_updated_formatted = $notes_updated->format('j F Y \a\t H:i');

		return compact('user');
	}
}

?>