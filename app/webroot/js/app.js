$(function(){

	$(document).keydown(function(e) {
		if((e.ctrlKey || e.metaKey) && (e.which == 83)) {
			e.preventDefault();
			$("input[name=save_notepad]").toggleClass('active');
			$("#notepad-form").submit();
			$("input[name=save_notepad]").toggleClass('active', 100);
			return false;
		}
		return true;
	});

	var note_content = $('#notes').val();
	$('#notes').bind('input propertychange', function() {
		$('#notepad-notice-edited').show();
		$('#notepad-notice-saved').hide();
		if ($(this).val()==note_content) {
			$('#notepad-notice-edited').hide();
		}

	});

	$('#notepad-form').on('submit', function(event) {
		event.preventDefault();
		$.ajax({
			type: 'post',
			url: $(this).attr('action'),
			data: $(this).serialize(),
			success: function(data) {
				$('#notepad-notice-edited').hide();

				$('#notepad-notice-saved').stop(true, true).show().fadeOut(1000);

				$('#edited-date').html(data);
				$('#edited-date-wrapper.hide').removeClass('hide');

				// $("#notes").stop(true, true);
				// $("#notes").toggleClass('green');
				// $("#notes").toggleClass('green', 1000);

				// $('#edited-date').hide().html(data).fadeIn(200);
			},
			error: function() {
				alert('There was an error saving a note.');
			}
		});
	});

	$('#notes').putCursorAtEnd();

	$('#Email').focus();
});

jQuery.fn.putCursorAtEnd = function() {
	return this.each(function() {
		$(this).focus()
		// If this function exists...
		if (this.setSelectionRange) {
			// ... then use it (Doesn't work in IE)
			// Double the length because Opera is inconsistent about whether a carriage return is one character or two. Sigh.
			var len = $(this).val().length * 2;
			this.setSelectionRange(len, len);
		} else {
			// ... otherwise replace the contents with itself
			// (Doesn't work in Google Chrome)
			$(this).val($(this).val()); 
		}
		// Scroll to the bottom, in case we're in a tall textarea
		// (Necessary for Firefox and Google Chrome)
		this.scrollTop = 999999;
	});
};
