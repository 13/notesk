<?php

namespace app\models;

class Users extends \lithium\data\Model {

	public $validates = array(
		'email' => array(
			array(
				'notEmpty',
				'message' => 'You must include an email.',
			),
		),
		'password' => array(
			array(
				'notEmpty',
				'message' => 'You must include a password.',
			),
		),
	);
}

?>